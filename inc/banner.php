        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Temos todo o tipo de Antenas Antifurto para Lojas</h2>
                <p>As antenas antifurto para lojas são tendências de mercado, principalmente pelos benefícios que proporcionam, como a redução considerável de perdas nesse tipo de estabelecimento, o que traz lucro.</p>
                <a href="<?=$url?>antenas-antifurto" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Procura Etiqueta de Segurança? Nós fazemos</h2>
                <p>A etiqueta de segurança é um utensílio que vem sendo amplamente requisitado pelos mais distintos segmentos de negócio na atualidade</p>
                <a href="<?=$url?>etiqueta-de-seguranca" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Temos Sistemas Antifurto também!</h2>
                <p>Os sistemas antifurto são hoje a maneira mais prática de lidar com o elevado índice de furtos que um estabelecimento de ordem comercial tem nos dias atuais, tendo em vista que se trata, de fato, de um modo eficaz de inibir esse tipo de ação</p>
                <a href="<?=$url?>sistemas-antifurto" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->